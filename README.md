# Angular 1 Devstack #

## Requirements 
- [git](http://git-scm.com/downloads)
- [nodeJS](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)
- [Java Development Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/index.html ) - for protractor

## Installation

Download this package

```shell
git clone git@bitbucket.org:przecza/feedback.git
```

Install dependencies

```shell
yarn install
```

or just 

```shell
yarn
```

note: npm is also working

## Development
Run example application with live reload

```shell
npm start
```

Open [http://localhost:8000/](http://localhost:8000/)in browser and happy developing

## Other commands
```
 npm start             # vývojový režim, spustí aplikaci lokálně, dělá livereload
 
 npm start -- --env testing # vývojový režim oproti testovacímu backendu
 
 npm start -- --env live    # vývojový režim oproti živému backendu
 
 npm start -- --env local    # vývojový režim oproti lokálnímu backendu
 
 npm test               # spuštění unit testů (karma)
 
 npm run test-watch     # opakovené spouštění unit testů při změně souborů 

 npm run protractor     # E2E testy
 
 npm build              # zbuilduje celou aplikaci do složky dist
```

## troubleshooting
list known issues here

## Missing project files in IntelliJ Idea

When project is pulled for the first time, it could happen, that the project files are not visible in Idea file tree.

To fix this, it is necessary to add project module. 

1) Open project settings (ctrl + alt + shift + S) and open modules submenu. 
2) Click green plus icon and select "Import module"
3) Select root folder of project
4) Select "Create module from existing source"
5) click next until finished

tip: do it before you perform module installation (yarn)


### Yarn for windows
 There was issue on windows, where yarn installed through msi installer put wrong path into system PATH variable 
 There was missing the \ at the end of path right after \bin
 
 before : C:\Program Files (x86)\Yarn\bin (Not working)
 After : C:\Program Files (x86)\Yarn\bin\ (Working)
 
 Tha path can differ if you have installed it only for your user.
