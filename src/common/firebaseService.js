angular.module('devFest2017.sessionFeedback.common.firebase', [])

  .factory('firebaseFactory', function ($log, $firebaseObject) {
    let firebaseFactory = {
      getReference: function (path) {
        return firebase.database().ref(path);
      },

      getObject: function (path) {
        const ref = this.getReference(path);
        return $firebaseObject(ref);
      },

      getObjectByRef: function (ref) {
        return $firebaseObject(ref);
      }
    };

    return firebaseFactory;
  });
