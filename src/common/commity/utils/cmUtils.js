angular.module('cm.common.utils', [])

  .service('cmUtilsService', function ($window, $log, $timeout) {

    /**
     * call callback function when resize events stop
     *
     * @param callback
     * @returns {{callback: *, clear: cmUtilService.clear}}
     */
    this.onWindowResize = function (callback) {
      let timeout = false;
      let delay = 100;

      if ($window.addEventListener) {
        $window.addEventListener('resize', () => {
          $timeout.cancel(timeout);
          timeout = $timeout(callback, delay);
        });
      } else {
        $log.error('ERROR: Failed to bind to window.resize with: ', callback);
      }
      // return object with clear function to remove the single added callback.
      return {
        callback: callback,
        clear: function () {
          $window.removeEventListener('resize', callback);
        }
      };
    };

  });
