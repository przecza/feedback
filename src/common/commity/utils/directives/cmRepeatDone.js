angular.module('cm.common.utils')

/**
 * directive to call any function after ng-repeat is done.
 */
  .directive('repeatDone', function () {
    return function (scope, element, attrs) {
      if (scope.$last) {
        scope.$eval(attrs.repeatDone);
      }
    };
  });
