angular.module('cm.common.utils')

/**
 * Capitalize first letter in String, and decapitate all other
 */
  .filter('capitalizeFirstLetterOnly', function () {
    return function (input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    };
  });
