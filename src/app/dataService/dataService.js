angular.module('devFest2017.sessionFeedback.dataService', [])

  .run(function (dataService) {
    dataService.initData();
  })

  .service('dataService', function (firebaseFactory, FIREBASE_SESSION_FEEDBACK, FIREBASE_SESSIONS, FIREBASE_SCHEDULE) {

    this.initData = function () {
      this.sessions = firebaseFactory.getObject(FIREBASE_SESSIONS);
      this.sessionsFeedbacks = firebaseFactory.getObject(FIREBASE_SESSION_FEEDBACK);
      this._scheduleRef = firebaseFactory.getReference(FIREBASE_SCHEDULE);
      this._getSaturdaySchedule();
    };

    this._getSaturdaySchedule = function () {
      this.scheduleObj = firebaseFactory.getObjectByRef(this._scheduleRef.child('0').child('timeslots'));
    };
  });
