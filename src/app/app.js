angular.module('devFest2017.sessionFeedback', [
  'ngMaterial',
  'firebase',
  'ui.router',
  'cm.common.utils',
  'cm.common.constants',
  'devFest2017.sessionFeedback.configuration',
  'devFest2017.sessionFeedback.common.firebase',
  'devFest2017.sessionFeedback.rate',
  'devFest2017.sessionFeedback.dataService'
])

  .constant('FIREBASE_SESSION_FEEDBACK', 'sessionsFeedbacks')
  .constant('FIREBASE_SESSIONS', 'sessions')
  .constant('FIREBASE_SCHEDULE', 'schedule')

  .config(function (FIREBASE_CONFIG) {
    firebase.initializeApp(FIREBASE_CONFIG);
  })

  .config(function ($urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
  })

  .run(function ($transitions) {
    //always go to home state on reload
    const destroyFirstLaunchTransitions = $transitions.onBefore({}, function(trans) {
      destroyFirstLaunchTransitions();
      return trans.router.stateService.target('home');
    });
  });

