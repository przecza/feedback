describe('application', function() {

  beforeEach(module('devFest2017.sessionFeedback'));

  it('should have defined module', function() {
    expect(angular.module('devFest2017.sessionFeedback')).toBeDefined();
  });

  it('should use ES6 syntax', function() {
    expect((function(a = 2) {
      return a;
    })()).toBe(2);
  });

});
