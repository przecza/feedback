angular.module('devFest2017.sessionFeedback.rate', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('rate', {
        url: '/rate/{sessionId}',
        component: 'stRate',
        resolve: {
          feedback: function (firebaseFactory, $stateParams, FIREBASE_SESSION_FEEDBACK) {
            const path = FIREBASE_SESSION_FEEDBACK + '/' + $stateParams.sessionId;
            return firebaseFactory.getObject(path);
          }
        }
      });
  })

  .component('stRate', {
    templateUrl: 'app/stRate/stRate.html',
    controller: RateController,
    bindings: {
      feedback: '<'
    }
  });

function RateController($state, $timeout) {
  this.HAPPY = 'happy';
  this.NAH = 'neutral';
  this.SAD = 'sad';

  this.show = {
    happy: false,
    neutral: false,
    sad: false
  };

  this.emotionURL = {
    happy: './images/1f60d.png',
    neutral: './images/1f610.png',
    sad: './images/1f621.png'
  };

  this.$onInit = function () {
    this._initSessionFeedbackNode();
  };

  this.doFeedback = function (emotion) {
    this.show[emotion] = true;
    this.feedback[emotion] += 1;
    this.feedback.$save();
    $timeout(()=> this.show[emotion] = false, 150);
  };

  this.close = function () {
    this.feedback['closed'] = moment().format('HH:mm:ss D-M-YYYY');
    this.feedback.$save();
    $state.go('home');
  };

  this._initSessionFeedbackNode = function () {
    this.feedback[this.HAPPY] = angular.isDefined(this.feedback[this.HAPPY]) ? this.feedback[this.HAPPY] : 0;
    this.feedback[this.NAH] = angular.isDefined(this.feedback[this.NAH]) ? this.feedback[this.NAH] : 0;
    this.feedback[this.SAD] = angular.isDefined(this.feedback[this.SAD]) ? this.feedback[this.SAD] : 0;
  };

}
