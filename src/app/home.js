angular.module('devFest2017.sessionFeedback')

  .config(function ($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        component: 'stHome'
      });
  })

  .component('stHome', {
    templateUrl: 'app/home.html',
    controller: HomeController
  });

function HomeController(dataService) {

  this.ROOMS = ['T1', 'E1', 'E2', 'Circle Hall'];

  this.$onInit = function () {
    this.scheduleObj = dataService.scheduleObj;
    this.sessions = dataService.sessions;
    this.sessionsFeedbacks = dataService.sessionsFeedbacks;
  };
}
